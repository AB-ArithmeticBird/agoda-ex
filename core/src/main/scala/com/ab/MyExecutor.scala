package com.ab

import java.util.concurrent._

import com.typesafe.config.ConfigFactory

object MyExecutor {
  val conf = ConfigFactory.load()
  private val numProcessor = Runtime.getRuntime.availableProcessors()
  private val factor = conf.getString("threadpool.factor").toInt
  private val noOfThread = numProcessor * factor
  val ioThreadPool: ExecutorService = Executors.newFixedThreadPool(noOfThread)

  val targetUtilization =
    conf.getString("threadpool.target-utilization").toFloat

  val ratioOfWaitTimeToComputeTime =
    conf.getString("threadpool.ratio-of-wait-time-to-compute-time").toFloat

  val numThreads = numProcessor * targetUtilization * (1 + ratioOfWaitTimeToComputeTime)

  val cpuThreadPool = new ThreadPoolExecutor(
    numThreads.toInt,
    numThreads.toInt,
    0L,
    TimeUnit.MILLISECONDS,
    new LinkedBlockingQueue[Runnable]())

}
