package com.ab.repository.interpreter

import java.util.UUID

import com.ab.model.{FileData, FileStatus}
import com.ab.repository.FileDataRepo
import scalikejdbc._

import scala.concurrent.{ExecutionContext, Future}

class FileDataInterpreter extends FileDataRepo {

  def allDownloadedFiles()(
      implicit e: ExecutionContext): Future[Seq[FileData]] = Future {
    DB localTx { implicit session =>
      sql"select * from filedata where filestatus >${FileStatus.Aborted.value} "
        .map(
          rs =>
            FileData(rs.string("id"),
                     rs.string("url"),
                     rs.stringOpt("filepath"),
                     FileStatus.withValue(rs.get[Int]("filestatus"))))
        .list()
        .apply()
    }
  }

  def byUrl(url: String)(
      implicit e: ExecutionContext): Future[Option[FileData]] = Future {
    DB localTx { implicit session =>
      sql"select * from filedata where url = ${url}"
        .map(
          rs =>
            FileData(rs.string("id"),
                     rs.string("url"),
                     rs.stringOpt("filepath"),
                     FileStatus.withValue(rs.get[Int]("filestatus"))))
        .single
        .apply()
    }
  }

  def byFilePath(filePath: String)(
      implicit e: ExecutionContext): Future[Option[FileData]] = Future {
    DB localTx { implicit session =>
      sql"select * from filedata where filepath = ${filePath}"
        .map(
          rs =>
            FileData(rs.string("id"),
                     rs.string("url"),
                     rs.stringOpt("filepath"),
                     FileStatus.withValue(rs.get[Int]("filestatus"))))
        .single
        .apply()
    }
  }
  def byId(id: String)(implicit e: ExecutionContext): Future[Option[FileData]] =
    Future {
      DB localTx { implicit session =>
        sql"select * from filedata where id = ${UUID.fromString(id)}"
          .map(
            rs =>
              FileData(rs.string("id"),
                       rs.string("url"),
                       rs.stringOpt("filepath"),
                       FileStatus.withValue(rs.get[Int]("filestatus"))))
          .single
          .apply()
      }
    }

  def insert(fileData: FileData)(implicit e: ExecutionContext): Future[Unit] =
    Future {
      DB localTx { implicit session =>
        val r = if (fileData.filePath.isDefined) fileData.filePath else null
        sql"INSERT INTO filedata(id, url, filepath, filestatus) VALUES (${UUID.fromString(
          fileData.id)}, ${fileData.url}, ${r} , ${fileData.status.value})".update
          .apply()
      }
    }

  def updateStatusByFilePath(status: FileStatus, filePath: String)(
      implicit e: ExecutionContext): Future[Unit] = Future {
    DB localTx { implicit session =>
      sql"UPDATE filedata SET filestatus =  ${status.value} where filepath = ${filePath}".update
        .apply()
    }
  }

  def updateStatusByUrl(status: FileStatus, url: String, filePath: String)(
      implicit e: ExecutionContext): Future[Unit] = Future {
    DB localTx { implicit session =>
      sql"UPDATE filedata SET filestatus =  ${status.value}, filepath = ${filePath} where url = ${url}".update
        .apply()
    }
  }

  override def updateStatusById(id: String, status: FileStatus)(
      implicit e: ExecutionContext): Future[Unit] = Future {
    DB localTx { implicit session =>
      sql"UPDATE filedata SET filestatus =  ${status.value} where id = ${UUID
        .fromString(id)}".update.apply()
    }
  }
}
