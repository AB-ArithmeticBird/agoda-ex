package com.ab.repository

import com.ab.model.{FileData, FileStatus}

import scala.concurrent.{ExecutionContext, Future}

trait FileDataRepo {

  def allDownloadedFiles()(implicit e: ExecutionContext): Future[Seq[FileData]]

  def byUrl(url: String)(implicit e: ExecutionContext): Future[Option[FileData]]

  def byFilePath(filePath: String)(
      implicit e: ExecutionContext): Future[Option[FileData]]

  def byId(id: String)(implicit e: ExecutionContext): Future[Option[FileData]]

  def insert(fileData: FileData)(implicit e: ExecutionContext): Future[Unit]

  def updateStatusById(id: String, status: FileStatus)(
      implicit e: ExecutionContext): Future[Unit]

  def updateStatusByFilePath(status: FileStatus, filePath: String)(
      implicit e: ExecutionContext): Future[Unit]

  def updateStatusByUrl(status: FileStatus, url: String, filePath: String)(
      implicit e: ExecutionContext): Future[Unit]

}
