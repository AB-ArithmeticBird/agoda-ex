package com.ab.model

sealed trait Address extends Product with Serializable
case class HttpAddress(address: String) extends Address

case class HttpsAddress(address: String) extends Address

case class FtpAddress(address: String) extends Address

object Address {
  import scala.util._
  def apply(fileUrl: String): Either[String, Address] =
    Try(new java.net.URI(fileUrl)).toOption match {
      case Some(u) =>
        u.getScheme.toLowerCase() match {
          case "http"  => Right(HttpAddress(fileUrl))
          case "https" => Right(HttpsAddress(fileUrl))
          case "ftp"   => Right(FtpAddress(fileUrl))
        }
      case None => Left("Unable to convert url to any address format")
    }
}

import enumeratum.values._

sealed abstract class FileStatus(val value: Int, val name: String)
    extends IntEnumEntry

case object FileStatus extends IntEnum[FileStatus] {

  case object UnInitialized
      extends FileStatus(value = 1, name = "UnInitialized")
  case object Downloading extends FileStatus(name = "Downloading", value = 2)
  case object Aborted extends FileStatus(3, "Aborted")
  case object ReadyForProcessing
      extends FileStatus(4, name = "ReadyForProcessing")
  case object Approved extends FileStatus(5, name = "Approved")
  case object Rejected extends FileStatus(6, name = "Rejected")

  val values = findValues

}

case class FileData(id: String,
                    url: String,
                    filePath: Option[String],
                    status: FileStatus)
