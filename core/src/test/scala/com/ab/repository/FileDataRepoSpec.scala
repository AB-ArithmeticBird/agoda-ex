package com.ab.repository

import java.util.UUID

import com.ab.model.{FileData, FileStatus}
import com.ab.repository.interpreter.FileDataInterpreter
import org.scalatest.AsyncFlatSpec
import scalikejdbc.{AutoSession, ConnectionPool, _}

class FileDataRepoSpec extends AsyncFlatSpec{

  // initialize JDBC driver & connection pool
  Class.forName("org.h2.Driver")
  ConnectionPool.singleton("jdbc:h2:mem:hello", "user", "pass")

  implicit val session: AutoSession.type = AutoSession

  sql"create table filedata (id UUID primary key, url varchar(2000) unique not null , filepath varchar(2000) unique , filestatus int)".execute.apply()

  behavior of "Repository"

  "insert" should "save the record of FileData" in  {
    sql"Delete from filedata".execute.apply()

    val interpreter = new FileDataInterpreter
    val f = FileData(UUID.randomUUID().toString,"http://alpha.txt", None, FileStatus.UnInitialized)
    interpreter.insert(f)
    val find = interpreter.byUrl("http://alpha.txt")
    find.map(ff => assert(ff.get.url.contains("http://alpha.txt")))
  }

  "update" should "update the status of file by file url" in  {
    sql"Delete from filedata".execute.apply()

    val interpreter = new FileDataInterpreter
    val f = FileData(UUID.randomUUID().toString,"http://alpha1.txt", Some("alpha1.txt"), FileStatus.ReadyForProcessing)
    val r = interpreter.insert(f)
    r.flatMap {  _ =>
      val find = interpreter.byFilePath("alpha1.txt")
      find.map(ff => assert(ff.get.filePath.contains("alpha1.txt")))
      val r2 = interpreter.updateStatusByUrl(FileStatus.Approved, "http://alpha1.txt", "alpha1.txt")
      r2.flatMap { _ =>
        val find2 = interpreter.byFilePath("alpha1.txt")
        find2.map(ff => assert(ff.get.status == FileStatus.Approved))
      }
    }
  }

  "update" should "update the status of file by file path" in  {
    sql"Delete from filedata".execute.apply()

    val interpreter = new FileDataInterpreter
    val f = FileData(UUID.randomUUID().toString,"http://alpha1.txt", Some("alpha1.txt"), FileStatus.ReadyForProcessing)
    val r = interpreter.insert(f)
    r.flatMap {  _ =>
      val find = interpreter.byFilePath("alpha1.txt")
      find.map(ff => assert(ff.get.filePath.contains("alpha1.txt")))
      val r2 = interpreter.updateStatusByFilePath(FileStatus.Approved, "alpha1.txt")
      r2.flatMap { _ =>
        val find2 = interpreter.byFilePath("alpha1.txt")
        find2.map(ff => assert(ff.get.status == FileStatus.Approved))
      }
    }
  }

  "update" should "update the status of file by file id" in  {
    sql"Delete from filedata".execute.apply()

    val interpreter = new FileDataInterpreter
    val uuid = UUID.randomUUID().toString
    val f = FileData(uuid,"http://alpha2.txt", Some("alpha2.txt"), FileStatus.ReadyForProcessing)
    val r = interpreter.insert(f)
    r.flatMap {  _ =>
      val find = interpreter.byId(uuid)
      find.map(ff => assert(ff.get.filePath.contains("alpha2.txt")))
      val r2 = interpreter.updateStatusById( uuid,FileStatus.Approved)
      r2.flatMap { _ =>
        val find2 = interpreter.byFilePath("alpha2.txt")
        find2.map(ff => assert(ff.get.status == FileStatus.Approved))
      }
    }
  }

  "allDownloadedFiles" should "return all files" in {
    sql"Delete from filedata".execute.apply()

    val interpreter = new FileDataInterpreter
    val f1 = FileData(UUID.randomUUID().toString,"http://alpha1.txt", None, FileStatus.Approved)
     interpreter.insert(f1)
    val f2 = FileData(UUID.randomUUID().toString,"http://alpha2.txt", None, FileStatus.Approved)
     interpreter.insert(f2)
    val f3 = FileData(UUID.randomUUID().toString,"http://alpha3.txt", None, FileStatus.UnInitialized)
     interpreter.insert(f3)
    val find = interpreter.allDownloadedFiles()
    find.map(ff => assert(ff.length == 2))
  }

}
