create table filedata
(
  url varchar(2000) not null,
  filepath varchar(2000),
  filestatus integer,
  id uuid not null
    constraint filedata_id_pk
    primary key
)
;

create unique index filedata_filepath_uindex
  on filedata (filepath)
;

