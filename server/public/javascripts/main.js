(function(){

    $().ready(function (e) {

        $('#doc-holder').hide();

        $("input:radio").on( "click", function( ev ) {
            var v = ev.currentTarget.value;
            var fileId = $(ev.currentTarget).parent().parent().attr('id');
            
            var o = {
                id:fileId,
                approve:v==="1"
            };
            var d = JSON.stringify(o);

            $.ajax({
                url: "/file/approve",
                contentType: 'application/json',
                dataType: 'json',
                data: d,
                type: 'POST',
                success: function(data, t, j){
                    alert(data.message);
                    $('#'+fileId).find('td.status').empty().append(document.createTextNode(data.data));
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                   alert(errorThrown);
                }
            });

        });

        function hexToBase64(str) {
            return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
        }

        $('a.find-file').click(function(e) {
            e.preventDefault();
            var fileId = $(e.currentTarget).parent().parent().attr('id');
            var fileName = e.currentTarget.innerText;
           
            var extension = fileName.split('.')[1];
            var spaceForDownloadFile = $("#doc-holder");
             if ((extension === 'txt')||(extension === 'html')||(extension === 'html')){
                    $.ajax({
                        url: "file/"+fileId,
                        cache: false,
                        success: function(data, textStatus, request) {
                            var ct = request.getResponseHeader('content-type');
                            if ((ct === 'txt/plain') || (ct === 'txt/html')) {
                                if (data) {
                                    var content = document.createTextNode(data);
                                    $('#doc-holder').empty().append(content).show();

                                } else {
                                    alert('Something went wrong');
                                }

                            }else {
                                alert('something wrong')
                            }
                        }
                    });
            }   else {

                 var iframe = $("<img/>").attr({
                     src: "file/" + fileId,
                 }).appendTo(spaceForDownloadFile);
                 $('#doc-holder').show();
             }
            
        } );
    }) ;
})();