import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.ab.model.{FileData, FileStatus}
import com.ab.repository.FileDataRepo
import controllers.HomeController
import org.scalatestplus.play._
import play.api.libs.json.Json
import play.api.mvc.{Results, _}
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.{ExecutionContext, Future}
class HomeControllerSpec extends PlaySpec with Results {

  "Home Page" should {
    "index should be valid" in {
      val controller = new HomeController(stubControllerComponents(), ActorSystem("Fake")
        ,new FakeRepo) (scala.concurrent.ExecutionContext.global)
      val result: Future[Result] = controller.index().apply(FakeRequest())
      val bodyText: String = contentAsString(result)
      bodyText.substring(0,9) mustBe "\n\n\n<!DOCT"
    }

    "download should be valid" in {

      implicit val sys = ActorSystem("Fake")
      val controller = new HomeController(stubControllerComponents(), sys
        ,new FakeRepo) (scala.concurrent.ExecutionContext.global)
      val req: FakeRequest[AnyContentAsJson] = FakeRequest().withHeaders(Seq(("content-type", "application/json")): _*)
        .withJsonBody(Json.parse("""{ "id": "1234","approve":false }"""))
      val result = controller.download("1").apply(req)
      implicit val mat = ActorMaterializer()
      implicit val timeOut = Timeout (10, TimeUnit.SECONDS)
      val bodyText = contentAsString(result)(timeOut,mat)
      bodyText mustBe "hi"
    }

    class FakeRepo extends  FileDataRepo{
      override def allDownloadedFiles()(implicit e: ExecutionContext): Future[Seq[FileData]]
      = Future.successful(Seq(FileData("1","fakeurl",Some("/Fina/agoda-ex/server/test/resources/a.txt"),FileStatus.Approved)))

      override def byUrl(url: String)(implicit e: ExecutionContext): Future[Option[FileData]] = Future(None)

      override def byFilePath(filePath: String)(implicit e: ExecutionContext): Future[Option[FileData]] = ???

      override def byId(id: String)(implicit e: ExecutionContext): Future[Option[FileData]]
      = Future.successful(Some(FileData("1","fakeurl",Some("/Users/arithmeticbird/Desktop/Fina/agoda-ex/server/test/resources/a.txt"),FileStatus.Approved)))

      override def insert(fileData: FileData)(implicit e: ExecutionContext): Future[Unit] = ???

      override def updateStatusById(id: String, status: FileStatus)(implicit e: ExecutionContext): Future[Unit] = Future.successful(())

      override def updateStatusByFilePath(status: FileStatus, filePath: String)(implicit e: ExecutionContext): Future[Unit] = ???

      override def updateStatusByUrl(status: FileStatus, url: String, filePath: String)(implicit e: ExecutionContext): Future[Unit] = ???
    }
  }
}