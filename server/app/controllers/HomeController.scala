package controllers

import java.io.File

import com.ab._
import akka.actor.ActorSystem
import com.ab.model.{FileData, FileStatus}
import com.ab.repository.FileDataRepo
import dto.{ErrorMessage, FileDataDTO, FileDataStatusDTO, SuccessfulMessage}
import javax.inject._
import play.api.Logger
import play.api.libs.json._
import play.api.mvc._
import scalaz.Scalaz._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

@Singleton
class HomeController @Inject()(
    cc: ControllerComponents,
    actorSystem: ActorSystem,
    repo: FileDataRepo)(implicit exec: ExecutionContext)
    extends AbstractController(cc) {

  import dto.Formatter._

  def index: Action[AnyContent] = Action.async {
    getFileData.map { f =>
      Ok(
        views.html.index("Hey Agoda, Your new application is ready :).",
                         f.toList))
    }
  }

  def approveDocument(): Action[JsValue] = Action.async(parse.json) { request =>
    val statusDTO = request.body.as[FileDataStatusDTO]

    val d = statusDTO.approve.fold(FileStatus.Approved, FileStatus.Rejected)

    val f = repo.updateStatusById(
      statusDTO.id,
      statusDTO.approve.fold(FileStatus.Approved, FileStatus.Rejected))(
      ExecutionContext.fromExecutor(MyExecutor.ioThreadPool))

    f map { _ =>
      Ok(Json.toJson(SuccessfulMessage("status updated", Option(d.name))))
    } recover {
      case NonFatal(e) =>
        Logger.warn(s"Converting a failed future to sensible error: $e")
        InternalServerError(
          Json.toJson(ErrorMessage("Unable to update status", None)))
    }
  }

  def download(id: String): Action[AnyContent] = Action.async {
    val c: Future[Option[FileData]] =
      repo.byId(id)(ExecutionContext.fromExecutor(MyExecutor.ioThreadPool))
    c.map { fl =>
      val fp = fl flatMap { fd =>
        fd.filePath
      }
      fp match {
        case Some(f) =>
          Ok.sendFile(
            content = new File(f),
            fileName = _ => new File(f).getName
          )
        case None => {
          Logger.error("File not found")
          InternalServerError(
            Json.toJson(ErrorMessage("Unable to send file", None)))
        }
      }
    }

  }

  private def getFileData: Future[Seq[FileDataDTO]] = {
    val allFiles = repo.allDownloadedFiles()(
      ExecutionContext.fromExecutor(MyExecutor.ioThreadPool))
    allFiles.map { f =>
      f.map { x =>
        FileDataDTO(x.id,
                    x.filePath.map(xx => new File(xx).getName),
                    x.status.name)
      }
    }
  }

}
