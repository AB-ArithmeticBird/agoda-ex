package dto

import play.api.libs.json.{Format, Json} // Combinator syntax

case class FileDataDTO(id: String, fileName: Option[String], status: String)

case class FileDataStatusDTO(id: String, approve: Boolean)

sealed trait ClientMessage
case class SuccessfulMessage(message: String, data: Option[String])
    extends ClientMessage
case class ErrorMessage(message: String, details: Option[String])
    extends ClientMessage

object Formatter {
  implicit lazy val fdFormat: Format[FileDataDTO] = Json.format[FileDataDTO]
  implicit lazy val stFormat: Format[FileDataStatusDTO] =
    Json.format[FileDataStatusDTO]
  implicit lazy val sFormat: Format[SuccessfulMessage] =
    Json.format[SuccessfulMessage]
  implicit lazy val eFormat: Format[ErrorMessage] = Json.format[ErrorMessage]
}
