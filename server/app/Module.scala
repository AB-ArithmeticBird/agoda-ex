import com.ab.repository.FileDataRepo
import com.ab.repository.interpreter.FileDataInterpreter
import com.google.inject.AbstractModule
import com.typesafe.config.ConfigFactory
import play.api.libs.concurrent.AkkaGuiceSupport
import scalikejdbc.ConnectionPool
class Module extends AbstractModule with AkkaGuiceSupport {

  override def configure() = {

    bind(classOf[FileDataRepo]).to(classOf[FileDataInterpreter])

    val c = ConfigFactory.load()
    ConnectionPool.singleton(c.getString("db.url"),
                             c.getString("db.user"),
                             c.getString("db.pass"))

  }

}
