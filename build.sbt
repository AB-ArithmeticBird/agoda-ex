import sbt.Keys._
import sbt.Project.projectToRef

name := "agoda-ex"

version := "0.1"

scalaVersion := "2.12.5"


lazy val `agoda-ex` = (project in file("."))
  .settings(moduleName := "agoda-ex")
  .aggregate(batch, server, core)
  .dependsOn(batch, server, core)

lazy val akkaVersion = "2.5.11"

lazy val core = (project in file("core"))
  .settings(
    libraryDependencies ++= Seq(
      "org.scalaz" %% "scalaz-core" % "7.2.21",
      "com.typesafe" % "config" % "1.3.2",
      "com.typesafe.akka" %% "akka-testkit" % "2.5.12" % Test,
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "com.beachape" %% "enumeratum" % "1.5.13",
      "org.scalikejdbc" %% "scalikejdbc"       % "3.2.1",
      "com.h2database"  %  "h2"                % "1.4.196" % Test,
      "org.postgresql" % "postgresql" % "9.4-1200-jdbc41" exclude("org.slf4j", "slf4j-simple"),
      "org.scalikejdbc" %% "scalikejdbc-config"  % "3.2.1",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "ch.qos.logback" % "logback-classic" % "1.2.3" % Test,
      "com.beachape" %% "enumeratum-play-json" % "1.5.14"
    )
  )


lazy val server = (project in file("server"))
  .dependsOn(core)
  .enablePlugins(PlayScala)
  .settings(
    routesGenerator := InjectedRoutesGenerator,
    libraryDependencies ++= Seq(
      ehcache ,
      ws ,
      guice,
      "com.typesafe" % "config" % "1.3.2",
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "org.scalaz" %% "scalaz-core" % "7.2.21" ,
      "com.typesafe.akka" %% "akka-testkit" % "2.5.12" % Test,
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "com.beachape" %% "enumeratum" % "1.5.13",
      "com.beachape" %% "enumeratum-play-json" % "1.5.13",
      "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test
    )
  )


lazy val batch = (project in file("batch"))
  .dependsOn(core)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.play" %% "play-ahc-ws-standalone" % "2.0.0-M1",
      "com.jsuereth" %% "scala-arm" % "2.0",
      "com.typesafe" % "config" % "1.3.2",
      "org.scalaz" %% "scalaz-core" % "7.2.21" ,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-testkit" % "2.5.12" % Test,
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "com.beachape" %% "enumeratum" % "1.5.13",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0"
    )
  )


///////////////////
// Global settings:
///////////////////

parallelExecution in Global := false