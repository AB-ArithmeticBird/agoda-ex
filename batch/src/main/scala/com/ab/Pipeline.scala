package com.ab

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}
import com.ab.model.FileStatus
import com.ab.repository.FileDataRepo
import com.ab.service.BatchAlgebra
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext

abstract class Pipeline(batchAlgebra: BatchAlgebra, repo: FileDataRepo) {

  def source(fileNames: Seq[String]): Source[String, NotUsed]

  def flow(batchSize: Int): Flow[String, Any, NotUsed]
}

class PipelineInterpreter(batchAlgebra: BatchAlgebra, repo: FileDataRepo)(
    implicit ec: ExecutionContext)
    extends Pipeline(batchAlgebra, repo)
    with StrictLogging {

  override def source(fileNames: Seq[String]): Source[String, NotUsed] =
    Source(fileNames.toList)

  override def flow(batchSize: Int): Flow[String, Any, NotUsed] =
    Flow[String]
      .mapAsync(batchSize) { url =>
        batchAlgebra.startProcess(url).run(repo).map(_ => url)
      }
      .mapAsync(batchSize) { uri =>
        batchAlgebra.downloadFile(uri).map(x => (x, uri))
      }
      .map { x =>
        x._1 match {
          case e @ Left(error) => logger.warn(error); (e, x._2)
          case r @ Right(f) =>
            if (f.exists) (r, x._2) else (Left("Error downloading file"), x._2)
        }
      }
      .map(f =>
        f._1 match {
          case Right(file) =>
            repo.updateStatusByUrl(FileStatus.ReadyForProcessing,
                                   f._2,
                                   file.getAbsolutePath)
          case Left(error) => logger.error(error)
      })
}
