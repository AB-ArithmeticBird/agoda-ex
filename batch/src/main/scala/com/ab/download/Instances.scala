package com.ab.download

import java.io.File
import java.net.URI

import akka.actor.ActorSystem
import akka.stream.Supervision.Decider
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.ByteString
import com.ab.model.{FtpAddress, HttpAddress, HttpsAddress}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import play.api.libs.ws.ahc.{AhcCurlRequestLogger, _}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.control.NonFatal

trait Instances extends StrictLogging {
  def syst: ActorSystem
  def config: Config

  def timeOut: Duration

  implicit val httpDownload: Downloader[HttpAddress, Future, File] =
    new Downloader[HttpAddress, Future, File] {

      val directory = config.getString("directory")

      override def download(x: HttpAddress): Future[File] = {
        val url = new URI(x.address).getPath
        val fileName = url.substring(url.lastIndexOf('/') + 1, url.length())
        val file = new File(s"$directory/$fileName")

        implicit val sys: ActorSystem = syst
        val decider: Decider = {
          case NonFatal(_) => file.delete(); Supervision.Stop;
        }

        implicit val materializer: ActorMaterializer = ActorMaterializer(
          ActorMaterializerSettings(sys).withSupervisionStrategy(decider)
        )
        implicit val executionContext: ExecutionContextExecutor = sys.dispatcher

        val ws: StandaloneAhcWSClient = StandaloneAhcWSClient()
        call(x.address, file, ws).andThen { case _ => ws.close() }
      }
    }

  implicit val httpsDownload: Downloader[HttpsAddress, Future, File] =
    new Downloader[HttpsAddress, Future, File] {

      val conf = ConfigFactory.load()
      val directory = conf.getString("directory")

      override def download(x: HttpsAddress): Future[File] = {
        val url = new URI(x.address).getPath
        val fileName = url.substring(url.lastIndexOf('/') + 1, url.length())
        val file = new File(s"$directory/$fileName")

        implicit val sys: ActorSystem = syst
        val decider: Supervision.Decider = {
          case NonFatal(_) => file.delete(); Supervision.Stop;
        }

        implicit val materializer: ActorMaterializer = ActorMaterializer(
          ActorMaterializerSettings(sys).withSupervisionStrategy(decider)
        )
        implicit val executionContext: ExecutionContextExecutor = sys.dispatcher
        val ws: StandaloneAhcWSClient = StandaloneAhcWSClient()
        call(x.address, file, ws).andThen { case _ => ws.close() }
      }
    }

  implicit val ftpDownload: Downloader[FtpAddress, Future, File] =
    new Downloader[FtpAddress, Future, File] {
      override def download(x: FtpAddress): Future[File] = ???
    }

  private def call(address: String, file: File, ws: StandaloneAhcWSClient)(
      implicit sys: ActorSystem,
      mat: ActorMaterializer,
      ec: ExecutionContextExecutor) = {

    val futureResponse = ws
      .url(address)
      .withRequestFilter(AhcCurlRequestLogger())
      .withRequestTimeout(timeOut)
      .stream()

    futureResponse.flatMap { res =>
      val mSize = res.headers.get("Content-Length") match {
        case Some(Seq(length)) => Some(length)
        case _                 => None
      }
      val outputStream = java.nio.file.Files.newOutputStream(file.toPath)

      // The sink that writes to the output stream
      val sink = Sink.foreach[ByteString] { bytes =>
        logger.debug("Writing the byte")
        outputStream.write(bytes.toArray)
      }

      // materialize and run the stream
      res.bodyAsSource
        .log("error logging")
        .runWith(sink)
        .andThen {
          case result =>
            // Close the output stream whether there was an error or not
            outputStream.close()
            mSize.foreach(size =>
              if (size.toLong != file.length()) {
                logger.warn(
                  s"Deleting the file ${file.getAbsolutePath} as download was interrupted")
                file.delete()
            })
            // Get the result or rethrow the error
            result.get
        }
        .map { _ =>
          file
        }
    }
  }
}
