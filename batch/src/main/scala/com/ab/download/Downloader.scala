package com.ab.download

import scala.language.higherKinds

//Download TypeClass
trait Downloader[A, F[_], R] {
  def download(x: A): F[R]
}

object Downloader {

  def apply[A, F[_], R](
      implicit instance: Downloader[A, F, R]): Downloader[A, F, R] = instance

  implicit class DownloaderOps[A](value: A) {
    def download[F[_], R](implicit tc: Downloader[A, F, R]): F[R] = {
      tc.download(value)
    }
  }
}
