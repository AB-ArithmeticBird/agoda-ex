package com.ab.service.interpreter

import java.io.File
import java.util.UUID

import com.ab.Batch.Instances._
import com.ab.download.Downloader._
import com.ab.model._
import com.ab.repository.FileDataRepo
import com.ab.service.BatchAlgebra
import com.typesafe.scalalogging.StrictLogging
import scalaz.Scalaz._
import scalaz._

import scala.concurrent.{ExecutionContext, Future}

class BatchInterpreter extends BatchAlgebra with StrictLogging {

  override def downloadFile(fileUrl: String)(
      implicit ec: ExecutionContext): Future[Either[String, File]] = {
    Address(fileUrl).map {
      case h @ HttpAddress(_)   => h.download
      case hs @ HttpsAddress(_) => hs.download
      case ft @ FtpAddress(_)   => ft.download
    }.sequenceU
  }

  override def startProcess(fileUrl: String)(
      implicit ec: ExecutionContext): Kleisli[Future, FileDataRepo, Unit] =
    Kleisli { repo: FileDataRepo =>
      logger.debug(
        s"Starting the process by getting the record by url $fileUrl")
      repo.byUrl(fileUrl).flatMap { fDataM =>
        fDataM.fold(
          repo.insert(
            FileData(UUID.randomUUID().toString,
                     fileUrl,
                     None,
                     FileStatus.UnInitialized))) { fData =>
          if ((fData.status == FileStatus.UnInitialized) || fData.status == FileStatus.Aborted)
            Future.successful(())
          else
            Future.failed(throw new Exception("Already Processed"))
        }
      }
    }
}
