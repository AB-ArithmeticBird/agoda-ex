package com.ab.service

import java.io.File

import com.ab.repository.FileDataRepo
import scalaz.Kleisli

import scala.concurrent.{ExecutionContext, Future}

trait BatchAlgebra {

  def downloadFile(fileUrl: String)(
      implicit ec: ExecutionContext): Future[Either[String, File]]

  def startProcess(fileUrl: String)(
      implicit ec: ExecutionContext): Kleisli[Future, FileDataRepo, Unit]

}
