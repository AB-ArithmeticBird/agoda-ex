package com.ab

import java.io.File

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.ab.download.Instances
import com.ab.repository.interpreter.FileDataInterpreter
import com.ab.service.BatchAlgebra
import com.ab.service.interpreter.BatchInterpreter
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import scalikejdbc.ConnectionPool

import scala.concurrent.duration.{Duration, _}
import scala.util.control.NonFatal

object Batch extends App with StrictLogging {

  implicit val system = ActorSystem("QuickStart")

  implicit val ec = system.dispatcher

  val decider: Supervision.Decider = {
    case NonFatal(_) => Supervision.Resume;
  }
  implicit val materializer: ActorMaterializer = ActorMaterializer(
    ActorMaterializerSettings(system).withSupervisionStrategy(decider)
  )

  val conf = ConfigFactory.load()

  object Instances extends Instances {
    override def syst: ActorSystem = system

    override def config: Config = conf

    override def timeOut: Duration = {

      val t = conf.getString("download-timeout")

      if (t == "Inf") Duration.Inf else t.toInt.seconds
    }
  }

  val BatchSize = conf.getString("batch-size").toInt

  ConnectionPool.singleton(conf.getString("db.url"),
                           conf.getString("db.user"),
                           conf.getString("db.pass"))
  val repo = new FileDataInterpreter

  val batchAlgebra: BatchAlgebra = new BatchInterpreter

  //IMPORTANT  -To test, Read the file from command line which contains a url per line
  //or uncomment the next two line after commenting this line
  val files: Seq[String] = readUrlsFromFile(args(0))

//  val files = Seq(
//    "https://landsat-pds.s3.amazonaws.com/c1/L8/139/045/LC08_L1TP_139045_20170304_20170316_01_T1/LC08_L1TP_139045_20170304_20170316_01_T1_B1.TIF",
//    "https://scontent-frt3-1.xx.fbcdn.net/v/t31.0-8/1014949_10200815424321268_2121172515_o.jpg?_nc_cat=0&oh=d47ba50ea9bec03fdf893d6b5a33812d&oe=5B6B6DE0"
//  )

  val pipeline = new PipelineInterpreter(batchAlgebra, repo)

  logger.debug("Starting the batch")

  val (_, done) = pipeline
    .source(files)
    .via(pipeline.flow(BatchSize))
    .toMat(Sink.ignore)(Keep.both)
    .run()

  done.andThen { case _ => system.terminate() }

  system.registerOnTermination {
    System.exit(0)
  }

  def readUrlsFromFile(fileName: String): Seq[String] = {
    import resource._
    (for (source <- managed(scala.io.Source.fromFile(new File(fileName))))
      yield {
        val l = for (line <- source.getLines) yield line
        l.toList
      }).opt.getOrElse(Seq.empty)
  }
}
