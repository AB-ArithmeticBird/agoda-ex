package com.ab

import akka.actor.ActorSystem
import com.ab.download.Instances
import com.ab.model.HttpAddress
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.FunSuite

import scala.concurrent.Await
import scala.concurrent.duration._


class DownloaderSpec extends FunSuite{

  implicit lazy val sys =  ActorSystem()

  test("Downloader should download the file"){
      object Instances extends Instances {
        override def syst: ActorSystem = sys

        override def config: Config = ConfigFactory.load()

        override def timeOut: Duration = Duration.Inf
      }
      val f = Instances.httpDownload.download(HttpAddress("https://landsat-pds.s3.amazonaws.com/c1/L8/139/045/LC08_L1TP_139045_20170304_20170316_01_T1/LC08_L1TP_139045_20170304_20170316_01_T1_B1.TIF"))
      val res = Await.result(f, 80 seconds)
       assert(res.exists())
    }

  test("Downloader should delete the file in case of partial download"){
    object Instances extends Instances {
      override def syst: ActorSystem = sys

      override def config: Config = ConfigFactory.load()

      override def timeOut: Duration = 15 seconds
    }
    val f = Instances.httpDownload.download(HttpAddress("https://landsat-pds.s3.amazonaws.com/c1/L8/139/045/LC08_L1TP_139045_20170304_20170316_01_T1/LC08_L1TP_139045_20170304_20170316_01_T1_B1.TIF"))
    val res = Await.result(f, 20 seconds)
    assert(!res.exists())
  }
}