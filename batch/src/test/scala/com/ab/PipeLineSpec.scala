package com.ab

import java.io.File

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.ab.model.{FileData, FileStatus}
import com.ab.repository.FileDataRepo
import com.ab.service.BatchAlgebra
import org.scalatest.FunSuite
import scalaz.Kleisli

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.control.NonFatal

class PipeLineSpec  extends FunSuite{
  import PipeLineSpec._
  implicit lazy val sys =  ActorSystem()
  val decider: Supervision.Decider = {
    case NonFatal(_) => Supervision.Resume;
  }
  implicit val materializer: ActorMaterializer = ActorMaterializer(
    ActorMaterializerSettings(sys).withSupervisionStrategy(decider)
  )

  test("Pipeline Source test"){
    val pipeLine =  new PipelineInterpreter(batchAlgebra, repo) (sys.dispatcher)
    val sourceUnderTest = pipeLine.source(Seq("alpha","beta","gamma", "alpha1","beta2","gamma2", "alpha3","beta3","gamma3"))
    val future = sourceUnderTest.take(4).runWith(Sink.seq)
    val result = Await.result(future, 3.seconds)
    assert(result == Seq("alpha","beta","gamma", "alpha1"))
  }

  test("Pipeline Sink test"){
    val pipeLine =  new PipelineInterpreter(batchAlgebra, repo) (sys.dispatcher)
    val flowUnderTest = pipeLine.flow(2)
    val future = Source(1 to 10).map(num => s"FileNo:$num").via(flowUnderTest).runWith(Sink.fold(Seq.empty[Any])(_ :+ _))

    val result = Await.result(future, 3.seconds)
    assert(result.size == 10)
  }

  test("Pipeline Sink test with failed downloads"){
    val pipeLine =  new PipelineInterpreter(batchAlgebraWithSomeFailedDownloadResult, repo) (sys.dispatcher)
    val flowUnderTest = pipeLine.flow(2)
    val future = Source(1 to 100).map{num => if (num%2 == 0) 1+"FileName.txt" else 2+"FileName.txt" }.via(flowUnderTest).runWith(Sink.fold(Seq.empty[Any])(_ :+ _))
    val result = Await.result(future, 3.seconds)
    assert(result.size == 50)
  }

  test("Pipeline Sink test with database down"){
    val pipeLine =  new PipelineInterpreter(batchAlgebraWithDatabaseDown, repo) (sys.dispatcher)
    val flowUnderTest = pipeLine.flow(2)
    val future = Source(1 to 100).map{num => if (num%2 == 0) 1+"FileName.txt" else 2+"FileName.txt" }.via(flowUnderTest).runWith(Sink.fold(Seq.empty[Any])(_ :+ _))
    val result = Await.result(future, 3.seconds)
    assert(result.isEmpty)
  }

}

object PipeLineSpec{
     val batchAlgebra = new BatchAlgebra{
       override def downloadFile(fileUrl: String)(implicit ec: ExecutionContext): Future[Either[String, File]] =
         Future.successful(Right(new File("sample file name")))

       override def startProcess(fileUrl: String)(implicit ec: ExecutionContext): Kleisli[Future, FileDataRepo, Unit] = Kleisli{repo =>
         Future.successful(())
       }
     }

  val repo = new FileDataRepo(){
    override def allDownloadedFiles()(implicit e: ExecutionContext): Future[Seq[FileData]] = ???

    override def byUrl(url: String)(implicit e: ExecutionContext): Future[Option[FileData]]
    = Future.successful(Option(FileData("1","alpha",Option("ttt"), FileStatus.ReadyForProcessing)))

    override def byFilePath(filePath: String)(implicit e: ExecutionContext): Future[Option[FileData]] = ???

    override def byId(id: String)(implicit e: ExecutionContext): Future[Option[FileData]] = ???

    override def insert(fileData: FileData)(implicit e: ExecutionContext): Future[Unit] = ???

    override def updateStatusById(id: String, status: FileStatus)(implicit e: ExecutionContext): Future[Unit] = ???

    override def updateStatusByFilePath(status: FileStatus, filePath: String)(implicit e: ExecutionContext): Future[Unit] = ???

    override def updateStatusByUrl(status: FileStatus, url: String, filePath: String)(implicit e: ExecutionContext): Future[Unit] = {
      Future.successful(())
    }
  }

  val batchAlgebraWithSomeFailedDownloadResult = new BatchAlgebra{
    override def downloadFile(fileUrl: String)(implicit ec: ExecutionContext): Future[Either[String, File]] = {
      if(fileUrl.startsWith("1")) Future.successful(Right(new File("1"))) else
        Future.failed(throw new Exception)
    }

    override def startProcess(fileUrl: String)(implicit ec: ExecutionContext): Kleisli[Future, FileDataRepo, Unit] = Kleisli{repo =>
      Future.successful(())
    }
  }

  val batchAlgebraWithDatabaseDown = new BatchAlgebra{
    override def downloadFile(fileUrl: String)(implicit ec: ExecutionContext): Future[Either[String, File]] =
      Future.successful(Right(new File("sample file name")))
    override def startProcess(fileUrl: String)(implicit ec: ExecutionContext): Kleisli[Future, FileDataRepo, Unit] = Kleisli{repo =>
      Future.failed(throw new Exception)
    }
  }
}
